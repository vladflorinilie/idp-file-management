FROM maven:3.6.3-jdk-8

ADD files /opt/filemanagement

WORKDIR /opt/filemanagement

RUN mvn install -DskipTests

EXPOSE 8080

WORKDIR /opt/filemanagement/target

ENTRYPOINT ["java", "-jar", "file-management-0.0.1-SNAPSHOT.jar"]