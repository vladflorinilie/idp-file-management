package ro.acs.idp.filemanagement.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileManager {

    @Value("${app.ud.files-folder}")
    private String filesFolder;

    @Value("${app.ud.original}")
    private String originalFlag;

    @Value("${app.ud.modified}")
    private String modifiedFlag;

    public void uploadFile(MultipartFile file, String username) throws IOException {
        Path filesPath = Paths.get(filesFolder, username);

        if (!Files.exists(filesPath)) {
            Files.createDirectories(filesPath);
        }

        String[] fileName = file.getOriginalFilename().split("\\.");
        String newOrigName = fileName[0] + originalFlag + fileName[1];
        String newModName = fileName[0] + modifiedFlag + fileName[1];
        Path image = Paths.get(filesPath.toString(), newOrigName);
        Path imageMod = Paths.get(filesPath.toString(), newModName);

        try (OutputStream os = Files.newOutputStream(image)) {
            os.write(file.getBytes());
        }

        Files.copy(image, imageMod, StandardCopyOption.REPLACE_EXISTING);
    }

    public InputStream downloadFile(String username, String imageName) throws IOException {
        Path filesPath = Paths.get(filesFolder, username);

        if (!Files.exists(filesPath)) {
            throw new IOException();
        }

        String[] fileName = imageName.split("\\.");
        String newOrigName = fileName[0] + modifiedFlag + fileName[1];
        Path image = Paths.get(filesPath.toString(), newOrigName);

        return new FileInputStream(image.toFile());
    }
}
