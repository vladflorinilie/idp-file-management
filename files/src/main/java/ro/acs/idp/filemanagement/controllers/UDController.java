package ro.acs.idp.filemanagement.controllers;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.acs.idp.filemanagement.services.FileManager;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class UDController {

    private FileManager fileManager;

    UDController(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    @PostMapping("/upload")
    public void uploadImage(@RequestParam("file") MultipartFile file,
                            @RequestParam("username") String username) throws IOException {
        fileManager.uploadFile(file, username);
    }

    @GetMapping("/download/{username}/{image}")
    public void downloadImage(@PathVariable("username") String username, @PathVariable("image") String imageName,
                              HttpServletResponse response) throws IOException {
        InputStream is = fileManager.downloadFile(username, imageName);
        IOUtils.copy(is, response.getOutputStream());
        is.close();
    }
}
